CC=gcc 
CFLAGS=-Wall

all: part1
part1: part1.o
part1.o: part1.c 

clean:
	rm -f part1 part1.o
run: part1
	./part1
