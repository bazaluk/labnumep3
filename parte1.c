/* EP3
 * Bruna Bazaluk M V
 * 9797002
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

double comp_simp(int n, double h, double (*table)[4] );
double comp_trap(int n, double h, double (*table) [4]);


int main(int argc, char *argv[])
{
	//FILE *fopen( const char * filename, const char * mode );
	FILE *fp = fopen(argv[1],"r");
	int n = 7;
	//getline(&buff, &size, stdin);
	double h = 5;
	int col = 4;
	double table[n][col];

	for (int i=0; i<n; i++)
	{
		 fscanf(fp, "%lf %lf %lf %lf", &table[i][0], &table[i][1], &table[i][2], &table[i][3]);
	}
	//printf("\n%lf %lf %lf %lf\n",table[1][0], table[1][1], table[1][2], table[1][3]);
	
	double simp = comp_simp(n,h,table);
	double trap = comp_trap(n,h,table);
	printf("\ntrapezio %lf\nsimpson %lf\n",trap, simp);
}

double lagrange_interp(double x, int n, double (*table)[4])
{
	double pn;
	double cima, baixo = 1;

	for(int k=0;k<=n;k++)
	{
		cima=1;
		baixo =1;
		for (int j=0;j<=n;j++)
		{
			if(k != j && table[j][0]!=0)
			{
				//printf("\nbla=%lf\n",table[j][0]);
				cima *= x - table[j][0];
				baixo *= table[k][0] - table[j][0];	
				//printf("\ncima %lf\n",cima);
			}
		}
		//printf("\ntablek3 = %lf\n",table[k][3]);
		if(table[k][3]!=0)
			pn += (cima/baixo)*table[k][3];
	}
	//printf("pn=%lf\n",pn);
	return pn;
}

double comp_trap(int n, double h, double (*table)[4])
{
	double y_1 = 0;
	double y1 = 0;
	double sum = 0;
	h=5;
	printf("antes do for");
	for(int x=5; x <= table[n][0]; x+=5)
	{
		printf("oi for");
		y_1 = lagrange_interp(x-5, n, table);
		y1 = lagrange_interp(x, n, table);

		sum += y_1 - y1;
		printf("\nsum -> %lf\n",sum);
	}
	printf("\ndepois do for");

	return ( (h/2)*sum );
}

double comp_simp(int n, double h, double (*table)[4])
{
	double t2k = 0;
	double t2k_1 = 0;

	for(int k=1; k <= (n/2); k++)
	{
		t2k += lagrange_interp(table[0][2*k],n,table);
		t2k_1 += lagrange_interp(table[0][2*k -1],n,table);
	}

	return ( (h/3)*( lagrange_interp(table[0][0],n,table) ) + t2k*2 + t2k_1*4 + lagrange_interp(table[0][n],n,table) );
}
/*
void getTable(FILE fp, )
{
	
}*/
